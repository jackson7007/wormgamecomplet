#include "Canhao.h"



Canhao::Canhao(ofVec2f position)
{
	quantidade15 = 3;
	quantidade30 = 2;
	quantidade45 = 1;

	vida = 3;
	_position.set(position.x, position.y);

	imagemCanhao.load("images/cannon.png");
	imagemCanhao.setAnchorPercent(0, 0.5f);

	imagembase.load("images/baseCannon.png");
	imagembase.setAnchorPercent(0.5f, 0.5f);

	imagemseta.load("images/seta.png");
	imagemseta.setAnchorPercent(0, 0.5f);
}

void Canhao::setAtirou(bool atirou)
{
	_atirou = atirou;
}

bool Canhao::getAtirou()
{
	return _atirou;
}


void Canhao::setVez(bool vez)
{
	_vez = vez;
}

bool Canhao::getVez()
{
	return _vez;
}

float Canhao::getAngle(ofVec2f vetor)
{
	angle = toDegrees(atan2(vetor.y, vetor.x));
	return angle;
}

float Canhao::toDegrees(float gd)
{
	return gd * 180/PI;
}

float Canhao::toRadians(float gr)
{
	return gr * PI/180;
}

void Canhao::update(float deltaTime)
{
}

void Canhao::draw()
{
	ofPushMatrix();
	ofTranslate(_position.x, _position.y);
	ofRotateZ(angle);
	imagemCanhao.draw(0, 0);
	ofPopMatrix();

	ofPushMatrix();
	ofTranslate(_position.x, _position.y);
	imagembase.draw(0, 0);
	ofPopMatrix();

	if (_vez) // desenha a seta da direcao da bala no canhao
	{
		ofSetColor(255, 255, 255);
		ofDrawLine(_position.x, _position.y, mousePosition.x, mousePosition.y);
		ofSetColor(255, 255, 255);

		ofPushMatrix();
		ofTranslate(mousePosition.x, mousePosition.y);
		ofRotateZ(angle);
		imagemseta.draw(0, 0);
		ofPopMatrix();
	}
}

float Canhao::movedMouse(int x, int y)
{
	mousePosition.set(x, y);
	vecMouse = mousePosition - _position;
	return getAngle(vecMouse);
}

float Canhao::magnitude()
{
	float mag = vecMouse.x * vecMouse.x + vecMouse.y + vecMouse.y;
	if (mag < 0) {
		mag *= -1;
	}
	return mag;
}
ofVec2f Canhao::getVecMouse()
{
	return vecMouse;
}

ofVec2f Canhao::getposition()
{
	return _position;
}

ofImage Canhao::getImage()
{
	return imagemCanhao;
}

Canhao::~Canhao()
{
}
