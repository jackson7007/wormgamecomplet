#pragma once
#include "src/ofApp.h"

class World
{
private:
	ofVec2f gravity;
	float wind;
	ofImage imagem;
	ofImage mountain;
public:
	World();
	bool gameOver;
	//-----------GRAVITY------------------
	void setGravity(int x, int y);
	ofVec2f getGravity();

	//-----------WIND---------------------
	void setWind(float w1, float w2);
	float getWind();

	void drawFloor();

	void drawMountain();

	ofImage getImageMountain();
	ofVec2f getPositionMountain();

	~World();
};

