#pragma once
#include "src/ofApp.h"
class Bala
{
private:
	ofVec2f _position;
	ofVec2f _originalPosition;
	float mass;
	ofImage cannonBall;
	float speed = 180;
	ofVec2f momentum;
	ofVec2f _gravity;
	float _wind;
public:
	Bala(int x, int y, ofVec2f gravity, float wind);
	void setMass(float _mass);
	void draw();
	void update(float deltaTime, ofVec2f vecmouseGet);
	void applyImpulse(float magnitude, float angleCanhao);
	bool colidiu(ofImage imageMountain, ofVec2f positionMountain);
	bool colidiuCanhao(ofImage imagemCanhao, ofVec2f positionCanahao);
	void setPosiotionOriginal();
	~Bala();
};

