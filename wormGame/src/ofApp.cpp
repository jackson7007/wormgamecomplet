#include "ofApp.h"
#include "../World.h"
#include "../Canhao.h"
#include "../Bala.h"
#include <stdio.h>
#include <string>

using namespace std;

World *world;
Bala *bala;
Canhao *canhao;
Canhao *canhao2;

float wind;

ofVec2f cPosition;
ofVec2f c2Position;
int step = 0;
ofVec2f vecmouseGet;
ofVec2f gravity;
float magnitude;
float angleCanhao;
ofImage imageWind;
ofVec2f imageWindVec;
//para saber qual peso foi selecionado
int option = 1;

//fonts
ofTrueTypeFont font;
ofTrueTypeFont explica;
ofTrueTypeFont gameoverString;

//--------------------------------------------------------------
void ofApp::setup(){
	font.loadFont("fonts/verdana.ttf", 15);
	explica.loadFont("fonts/verdana.ttf", 20);
	gameoverString.loadFont("fonts/verdana.ttf", 60);
	world = new World();
	world->setGravity(0, 60);
	gravity = world->getGravity();

	//definir de que valor ate que valor o vento vai
	world->setWind(-9, 9);

	// valor do vento multiplicado por 10
	wind = world->getWind()*10;

	//carregar imagem de vento caso pra direita
	if (wind > 0)
	{
		imageWind.load("images/left_wind.png");
		imageWindVec.set(100, 180);

	}
	else //carregar imagem de vento caso pra esquerda
	{
		imageWind.load("images/right_wind.png");
		imageWindVec.set(ofGetWindowWidth() - imageWind.getWidth()-50, 180);
	}

	//posicao do canhao 1
	cPosition.set(60, ofGetWindowHeight() - 45);
	canhao = new Canhao(cPosition);

	//posicao do canhao 2
	c2Position.set(ofGetWindowWidth() - 60, ofGetWindowHeight() - 45);
	canhao2 = new Canhao(c2Position);

	//posicao inicial da bola
	bala = new Bala(60, ofGetWindowHeight() - 45, gravity, wind);

	//setVez par saber em quem desenhar a flecha que sai do canhao para apontar direcao
	canhao->setVez(true);
	canhao2->setVez(false);
}

//--------------------------------------------------------------
void ofApp::update(){
	//update dos canhoes
	canhao->update(ofGetLastFrameTime());
	canhao2->update(ofGetLastFrameTime());

	//verificar se canaho 1 atirou e nao eh fim de jogo
	if (canhao->getAtirou() && !world->gameOver)
	{
		//atualiza bla do canhao 1
		bala->update(ofGetLastFrameTime(), vecmouseGet);

		//valida se canhao 1 colidiu com as borda da tela ou colidiu com a montanha
		if (bala->colidiu(world->getImageMountain(), world->getPositionMountain()) && step % 2 == 0)
		{
			//se foi opcao 1 escolheu 15 kg desconta da quantidade
			if (option == 1 && canhao->quantidade15 > 0)
			{
				canhao->quantidade15 -= 1;
			}

			//se foi opcao 2 escolheu 30 kg desconta da quantidade
			if (option == 2 && canhao->quantidade30 > 0)
			{
				canhao->quantidade30 -= 1;
			}

			//se foi opcao 3 escolheu 45 kg desconta da quantidade
			if (option == 3 && canhao->quantidade45 > 0)
			{
				canhao->quantidade45 -= 1;
			}

			//setar como nao atirando mais
			canhao->setAtirou(false);
			// coloca a bola na posicao original antes do tiro
			bala->setPosiotionOriginal();
			// isso eh para informar que vai para o proximo jogador
			step += 1;
			//tira a flecha de direcao do canhao 1
			canhao->setVez(false);
			//adiciona a flecha de direcao do canhao 2
			canhao2->setVez(true);

			//muda vento
			world->setWind(-9, 9);
			wind = world->getWind() * 10;

			//coloca a bola do lado do canhao 2
			bala = new Bala(ofGetWindowWidth() - 60, ofGetWindowHeight() - 45, gravity, wind);

			//carrega imgem do vento e sua direcao
			if (wind > 0)
			{
				imageWind.load("images/left_wind.png");
				imageWindVec.set(100, 180);

			}
			else
			{
				imageWind.load("images/right_wind.png");
				imageWindVec.set(ofGetWindowWidth() - imageWind.getWidth() - 50, 180);
			}
		}

		//valida se a bala colidiu com o  canhao 2
		if (bala->colidiuCanhao(canhao2->getImage(), canhao2->getposition()) && step % 2 == 0)
		{
			//se foi opcao 1 escolheu 15 kg desconta da quantidade e desconta vida
			if (option == 1 && canhao->quantidade15 > 0)
			{
				canhao->quantidade15 -= 1;
				canhao2->vida -= 1;
			}

			//se foi opcao 2 escolheu 30 kg desconta da quantidade e desconta vida
			if (option == 2 && canhao->quantidade30 > 0)
			{
				canhao->quantidade30 -= 1;
				canhao2->vida -= 2;
			}

			//se foi opcao 3 escolheu 45 kg desconta da quantidade e desconta vida
			if (option == 3 && canhao->quantidade45 > 0)
			{
				canhao->quantidade45 -= 1;
				canhao2->vida -= 3;
			}

			//marca como nao atirando
			canhao->setAtirou(false);
			//coloca a bola na posicao original
			bala->setPosiotionOriginal();
			//para mudar de player
			step += 1;
			//seta vai aparecer no canha 2
			canhao->setVez(false);
			canhao2->setVez(true);

			//muda direcao do vento
			world->setWind(-9, 9);
			wind = world->getWind() * 10;

			//coloca a bola do lado do canhao dois
			bala = new Bala(ofGetWindowWidth() - 60, ofGetWindowHeight() - 45, gravity, wind);

			//valida lado do vento pra desenhar imagem
			if (wind > 0)
			{
				imageWind.load("images/left_wind.png");
				imageWindVec.set(100, 180);

			}
			else
			{
				imageWind.load("images/right_wind.png");
				imageWindVec.set(ofGetWindowWidth() - imageWind.getWidth() - 50, 180);
			}
		}

	}
	//caso canhao 2 atirou
	if (canhao2->getAtirou() && !world->gameOver)
	{
		//atualiza posicao da bola
		bala->update(ofGetLastFrameTime(), vecmouseGet);
		
		//valida se bola colidiu com a montanha ou com as bordas da janela
		if (bala->colidiu(world->getImageMountain(), world->getPositionMountain()) && step % 2 != 0)
		{
			//se escolheu 15 kg desconta 1 de quantidade
			if (option == 1 && canhao2->quantidade15 > 0)
			{
				canhao2->quantidade15 -= 1;
			}

			//se escolheu 30 kg desconta 1 de quantidade
			if (option == 2 && canhao2->quantidade30 > 0)
			{
				canhao2->quantidade30 -= 1;
			}

			//se escolheu 45 kg desconta 1 de quantidade
			if (option == 3 && canhao2->quantidade45 > 0)
			{
				canhao2->quantidade45 -= 1;
			}
			//colocar como atirou false
			canhao2->setAtirou(false);
			//coloca na posicao original
			bala->setPosiotionOriginal();
			//para mudar de jogador
			step += 1;
			//para mudar e a seta ser desenhada do canhao 1
			canhao->setVez(true);
			canhao2->setVez(false);
			//atualiza posicao e valor do vento
			world->setWind(-9, 9);
			wind = world->getWind() * 10;
			//coloca posicao da bala
			bala = new Bala(60, ofGetWindowHeight() - 45, gravity, wind);

			//defini lado que a imgem do vento vai ser desenhado
			if (wind > 0)
			{
				imageWind.load("images/left_wind.png");
				imageWindVec.set(100, 200);

			}
			else
			{
				imageWind.load("images/right_wind.png");
				imageWindVec.set(ofGetWindowWidth() - imageWind.getWidth() - 50, 200);
			}
		}

		//valida colisao de bala com canhao 1
		if (bala->colidiuCanhao(canhao->getImage(), canhao->getposition()) && step % 2 != 0)
		{
			//se escolheu 15 kg desconta 1 de quantidade e vida
			if (option == 1 && canhao2->quantidade15 > 0)
			{
				canhao2->quantidade15 -= 1;
				canhao->vida -= 1;
			}
			//se escolheu 30 kg desconta 1 de quantidade e vida
			if (option == 2 && canhao2->quantidade30 > 0)
			{
				canhao2->quantidade30 -= 1;
				canhao->vida -= 2;
			}
			//se escolheu 1-45 kg desconta 1 de quantidade e vida
			if (option == 3 && canhao2->quantidade45 > 0)
			{
				canhao2->quantidade45 -= 1;
				canhao->vida -= 3;
			}

			//marcar canhao 2 como nao atirando
			canhao2->setAtirou(false);
			//posicao original da bola
			bala->setPosiotionOriginal();
			//proximo player
			step += 1;
			//em quem vai aparecer a seta
			canhao->setVez(true);
			canhao2->setVez(false);
			//novo valor de vento
			world->setWind(-9, 9);
			wind = world->getWind() * 10;
			//nova posicao da bola
			bala = new Bala(60, ofGetWindowHeight() - 45, gravity, wind);
			//imagem que o vento vai desenhar
			if (wind > 0)
			{
				imageWind.load("images/left_wind.png");
				imageWindVec.set(100, 200);

			}
			else
			{
				imageWind.load("images/right_wind.png");
				imageWindVec.set(ofGetWindowWidth() - imageWind.getWidth() - 50, 200);
			}
		}
	}
	//caso canhao 1 ou 2 nao tever vida ou nenhum dos dois tiver bala game over
	if (canhao->vida <= 0 ||  canhao2->vida <= 0 || ((canhao->quantidade15 <= 0 && canhao->quantidade30 <= 0 && canhao->quantidade45 <= 0) && (canhao2->quantidade15 <= 0 && canhao2->quantidade30 <= 0 && canhao2->quantidade45 <= 0)))
	{
		world->gameOver = true;

	}

}

//--------------------------------------------------------------
void ofApp::draw(){
	//desenha fundo
		ofSetBackgroundColor(0, 255, 255, 255);
		if (!world->gameOver)
		{
			//desenha vento
			imageWind.draw(imageWindVec.x, imageWindVec.y);
			ofSetColor(0, 0, 0);
			if (wind > 0) 
			{
				font.drawString("Vento: " + to_string(wind) + " KM", imageWindVec.x, imageWindVec.y + 100);
			}
			else
			{
				font.drawString("Vento: " + to_string(wind*-1) + " KM", imageWindVec.x-60, imageWindVec.y + 100);
			}
			ofSetColor(255, 255, 255);

			//desenha opcoes kg
			ofSetColor(0, 0, 0);
			font.drawString("Digite(1) = 15kg, Digite(2) = 30kg, Digite(3) = 45Kg", 50, 60);
			ofSetColor(255, 255, 255);

			//desenha a vida do canahao 1
			ofSetColor(0, 0, 0);
			font.drawString("VIDAS: " + to_string(canhao->vida), 50, 800);
			ofSetColor(255, 255, 255);
			
			//desenha a vida do canhao dois
			ofSetColor(0, 0, 0);
			font.drawString("VIDAS: " + to_string(canhao2->vida), ofGetWindowWidth()-150, 800);
			ofSetColor(255, 255, 255);
			
			//caso escolheu 15 kg
			if (option == 1)
			{
				//escreve opcao 15 kg
				ofSetColor(255, 0, 0);
				font.drawString("15 kg", 100, 120);
				ofSetColor(255, 255, 255);

				//desenha quantidade do  canhao 1
				if (step % 2 == 0)
				{
					ofSetColor(255, 0, 0);
					font.drawString("qtd: " + to_string(canhao->quantidade15), 100, 170);
					ofSetColor(255, 255, 255);
				}
				else // desenha quantidade do canhao 2
				{
					ofSetColor(255, 0, 0);
					font.drawString("qtd: " +  to_string(canhao2->quantidade15), 100, 170);
					ofSetColor(255, 255, 255);
				}

				//escreve opcao 30 kg
				ofSetColor(0, 0, 0);
				font.drawString("30 kg", 200, 120);
				ofSetColor(255, 255, 255);

				//desenha quantidade do  canhao 1
				if (step % 2 == 0)
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao->quantidade30), 200, 170);
					ofSetColor(255, 255, 255);
				}
				else // desenha quantidade do canhao 2
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao2->quantidade30), 200, 170);
					ofSetColor(255, 255, 255);
				}

				//escreve opcao 45 kg
				ofSetColor(0, 0, 0);
				font.drawString("45 kg", 300, 120);
				ofSetColor(255, 255, 255);

				//desenha quantidade do  canhao 1
				if (step % 2 == 0)
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao->quantidade45), 300, 170);
					ofSetColor(255, 255, 255);
				}
				else // desenha quantidade do canhao 2
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao2->quantidade45), 300, 170);
					ofSetColor(255, 255, 255);
				}
			}

			if (option == 2)
			{
				//escreve opcao 15 kg
				ofSetColor(0, 0, 0);
				font.drawString("15 kg", 100, 120);
				ofSetColor(255, 255, 255);

				//desenha quantidade do  canhao 1
				if (step % 2 == 0)
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao->quantidade15), 100, 170);
					ofSetColor(255, 255, 255);
				}
				else // desenha quantidade do canhao 2
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao2->quantidade15), 100, 170);
					ofSetColor(255, 255, 255);
				}

				//escreve opcao 30 kg
				ofSetColor(255, 0, 0);
				font.drawString("30 kg", 200, 120);
				ofSetColor(255, 255, 255);

				//desenha quantidade do  canhao 1
				if (step % 2 == 0)
				{
					ofSetColor(255, 0, 0);
					font.drawString("qtd: " +  to_string(canhao->quantidade30), 200, 170);
					ofSetColor(255, 255, 255);
				}
				else // desenha quantidade do canhao 2
				{
					ofSetColor(255, 0, 0);
					font.drawString("qtd: " +  to_string(canhao2->quantidade30), 200, 170);
					ofSetColor(255, 255, 255);
				}

				//escreve opcao 45 kg
				ofSetColor(0, 0, 0);
				font.drawString("45 kg", 300, 120);
				ofSetColor(255, 255, 255);

				//desenha quantidade do  canhao 1
				if (step % 2 == 0)
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao->quantidade45), 300, 170);
					ofSetColor(255, 255, 255);
				}
				else // desenha quantidade do canhao 2
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao2->quantidade45), 300, 170);
					ofSetColor(255, 255, 255);
				}
			}

			if (option == 3)
			{
				//escreve opcao 15 kg
				ofSetColor(0, 0, 0);
				font.drawString("15 kg", 100, 120);
				ofSetColor(255, 255, 255);

				//desenha quantidade do  canhao 1
				if (step % 2 == 0)
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao->quantidade15), 100, 170);
					ofSetColor(255, 255, 255);
				}
				else // desenha quantidade do canhao 2
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao2->quantidade15), 100, 170);
					ofSetColor(255, 255, 255);
				}

				//escreve opcao 30 kg
				ofSetColor(0, 0, 0);
				font.drawString("30 kg", 200, 120);
				ofSetColor(255, 255, 255);

				//desenha quantidade do  canhao 1
				if (step % 2 == 0)
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao->quantidade30), 200, 170);
					ofSetColor(255, 255, 255);
				}
				else // desenha quantidade do canhao 2
				{
					ofSetColor(0, 0, 0);
					font.drawString("qtd: " +  to_string(canhao2->quantidade30), 200, 170);
					ofSetColor(255, 255, 255);
				}

				//escreve opcao 45 kg
				ofSetColor(255, 0, 0);
				font.drawString("45 kg", 300, 120);
				ofSetColor(255, 255, 255);

				//desenha quantidade do  canhao 1
				if (step % 2 == 0)
				{
					ofSetColor(255, 0, 0);
					font.drawString("qtd: " +  to_string(canhao->quantidade45), 300, 170);
					ofSetColor(255, 255, 255);
				}
				else // desenha quantidade do canhao 2
				{
					ofSetColor(255, 0, 0);
					font.drawString("qtd: " +  to_string(canhao2->quantidade45), 300, 170);
					ofSetColor(255, 255, 255);
				}
			}
			world->drawMountain();
			canhao->draw();
			canhao2->draw();
			world->drawFloor();

			if (canhao->getAtirou()) {
				bala->draw();
			}

			if (canhao2->getAtirou()) {
				bala->draw();
			}

		}
		else //caso de game over
		{
			ofSetColor(0, 0, 0);
			gameoverString.drawString("GAME OVER", ofGetWindowWidth() / 3, ofGetWindowHeight() / 2);
			ofSetColor(255, 255, 255);

			if (canhao2->vida <= 0)
			{
				ofSetColor(0, 0, 0);
				gameoverString.drawString("PLAYER 1 VENCEU", ofGetWindowWidth() / 4, 800);
				ofSetColor(255, 255, 255);
			}

			if (canhao->vida <= 0)
			{
				ofSetColor(0, 0, 0);
				gameoverString.drawString("PLAYER 2 VENCEU", ofGetWindowWidth() / 4, 800);
				ofSetColor(255, 255, 255);
			}

			if ((canhao->quantidade15 <= 0 && canhao->quantidade30 <= 0 && canhao->quantidade45 <=0) && (canhao2->quantidade15 <= 0 && canhao2->quantidade30 <= 0 && canhao2->quantidade45 <= 0))
			{
				ofSetColor(0, 0, 0);
				gameoverString.drawString("EMPATE", ofGetWindowWidth() / 2, 800);
				ofSetColor(255, 255, 255);
			}

			ofSetColor(0, 0, 0);
			explica.drawString("Aperte [ENTER] para reiniciar", ofGetWindowWidth() / 3, 900);
			ofSetColor(255, 255, 255);
		}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (!canhao->getAtirou() && !canhao2->getAtirou() && !world->gameOver)
	{
		if (key == 49)// tecal 1
		{

			if(canhao->getVez())
			{
				if (canhao->quantidade15 > 0) {
					option = 1;
					bala->setMass(15);//muda peso para 15
				}
			}
			else
			{
				if (canhao2->quantidade15 > 0) {
					option = 1;
					bala->setMass(15);//muda peso para 15
				}
			}
		}

		if (key == 50) // tecla 2
		{

			if (canhao->getVez())
			{
				if (canhao->quantidade30 > 0) {
					option = 2;
					bala->setMass(30); //muda peso para 30
				}
			}
			else
			{
				if (canhao2->quantidade30 > 0) {
					option = 2;
					bala->setMass(30); //muda peso para 30
				}
			}

		}

		if (key == 51) // tecla 3
		{

			if (canhao->getVez())
			{
				if (canhao->quantidade45 > 0) {
					option = 3;
					bala->setMass(45);//muda peso para 45
				}
			}
			else
			{
				if (canhao2->quantidade45 > 0) {
					option = 3;
					bala->setMass(45);//muda peso para 45
				}
			}


		}
	}

	if (key == 13 && world->gameOver) // enter caso game over
	{
		//reseta valores para reiniciar o jogo
		world->gameOver = false;
		canhao->vida = 3;
		canhao2->vida = 3;
		step = 0;
		canhao->setVez(true);
		canhao2->setVez(false);
		world->setWind(-9, 9);
		wind = world->getWind() * 10;
		bala = new Bala(60, ofGetWindowHeight() - 45, gravity, wind);
		canhao->setAtirou(false);
		canhao2->setAtirou(false);

		canhao->quantidade15 = 3;
		canhao->quantidade30 = 2;
		canhao->quantidade45 = 1;

		canhao2->quantidade15 = 3;
		canhao2->quantidade30 = 2;
		canhao2->quantidade45 = 1;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
	if (!world->gameOver)
	{
		if (step % 2 == 0)
		{
			//pega posicao do mouse pra criar vetor de posicao de canhao ate mouse e tira o angulo pra rotacionar o canhao 1
			canhao->movedMouse(x, y);
		}
		else
		{
			//pega posicao do mouse pra criar vetor de posicao de canhao ate mouse e tira o angulo pra rotacionar o canhao 2
			canhao2->movedMouse(x, y);
		}
	}
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	//caso clicou para atirar
	if (!world->gameOver)
	{
		if (step % 2 == 0)//canhao 1
		{
			if (!canhao->getAtirou())
			{
				vecmouseGet = canhao->getVecMouse(); // pega a posicao do mouse para mandar no update da bala
				angleCanhao = canhao->movedMouse(x, y); //angulo do canhao
				magnitude = canhao->magnitude() / 10; //forca do tiro
				bala->applyImpulse(magnitude, angleCanhao); //impulso
			}
			canhao->setAtirou(true);//marca canhao 1 com tirando
			canhao2->setAtirou(false);
		}
		else//canhao 2
		{
			if (!canhao2->getAtirou())
			{
				//mesca coisa que em cim mais para o canhao 2
				vecmouseGet = canhao2->getVecMouse();
				angleCanhao = canhao2->movedMouse(x, y);
				magnitude = canhao2->magnitude() / 10;
				bala->applyImpulse(magnitude, angleCanhao);
			}
			canhao2->setAtirou(true);
			canhao->setAtirou(false);
		}
	}

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
