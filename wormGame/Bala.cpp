#include "Bala.h"
#include <stdio.h>

using namespace std;

Bala::Bala(int x, int y, ofVec2f gravity, float wind)
{
	_position.set(x, y);
	_originalPosition = _position;
	cannonBall.load("images/cannon-ball.png");
	cannonBall.setAnchorPercent(0.5f, 0.5f);
	momentum.set(0, 0);
	mass = 15;
	_gravity.set(gravity.x, gravity.y);
	_wind = wind;
}

void Bala::setMass(float _mass)
{
	mass = _mass;
}

void Bala::draw()
{
	ofPushMatrix();
	ofTranslate(_position.x, _position.y);
	cannonBall.draw(0, 0);
	ofPopMatrix();
}

void Bala::update(float deltaTime, ofVec2f vecmouseGet)
{
		ofVec2f forces;
		ofVec2f acceleration;
		ofVec2f accelSecs;
		ofVec2f rotationVec;

		forces += vecmouseGet.normalize() * speed;
		forces.x += _wind;

		acceleration = forces / mass;
		accelSecs = (acceleration + _gravity) * deltaTime;

		_position += (momentum + accelSecs) * deltaTime;
		momentum += accelSecs;
}

void Bala::applyImpulse(float magnitude, float angleCanhao) {
	momentum.set(0, 0);
	ofVec2f impulse;
	impulse.set(magnitude * cos(angleCanhao * PI / 180), magnitude * sin(angleCanhao * PI / 180));
	momentum += impulse / mass;
}

bool Bala::colidiu(ofImage imageMountain, ofVec2f positionMountain)
{
	//colide com bordas
	if (_position.x < 0 || _position.x > ofGetWindowWidth()) return true;

	if (_position.y > ofGetWindowHeight()) return true;

	//bound box colisao com a montanha
	if (_position.x < positionMountain.x + imageMountain.getWidth() &&
		positionMountain.x < _position.x + cannonBall.getWidth() &&
		_position.y < positionMountain.y + imageMountain.getHeight() &&
		positionMountain.y < _position.y + cannonBall.getHeight())
	{

		return true;
	}

	return false;
}

bool Bala::colidiuCanhao(ofImage imagemCanhao, ofVec2f positionCanahao)
{
	//bound box colisao com o canhao
	if (_position.x < positionCanahao.x + imagemCanhao.getWidth() &&
		positionCanahao.x < _position.x + cannonBall.getWidth() &&
		_position.y < positionCanahao.y + imagemCanhao.getHeight() &&
		positionCanahao.y < _position.y + cannonBall.getHeight())
	{
		return true;
	}

	return false;
}

void Bala::setPosiotionOriginal()
{
	_position = _originalPosition;
	momentum.set(0, 0);
}

Bala::~Bala()
{
}
