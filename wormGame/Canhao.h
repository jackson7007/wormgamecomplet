#pragma once
#include "src/ofApp.h"
class Canhao
{
private:
	float angle = 0;
	float force;
	ofVec2f _position;
	ofVec2f normalize;
	ofImage imagemCanhao;
	ofImage imagembase;
	ofImage imagemseta;
	ofVec2f mousePosition;
	ofVec2f vecMouse;
	bool _vez = false;
	bool _atirou = false;

public:
	int vida;
	int quantidade15;
	int quantidade30;
	int quantidade45;
	Canhao(ofVec2f position);
	void setVez(bool vez);
	bool getVez();
	void setAtirou(bool atirou);
	bool getAtirou();
	float getAngle(ofVec2f vetor);
	float toDegrees(float gd);
	float toRadians(float gr);
	void update(float deltaTime);
	float movedMouse(int x, int y);
	ofVec2f getVecMouse();
	ofVec2f getposition();
	ofImage getImage();
	float magnitude();
	void draw();
	~Canhao();
};
