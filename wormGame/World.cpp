#include "World.h"



World::World()
{
	mountain.load("images/mountain.png");
	gameOver = false;
}

void World::setGravity(int x, int y)
{
	gravity.set(x, y);
}

ofVec2f World::getGravity()
{
	return gravity;
}

void World::setWind(float w1, float w2)
{
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = w2 - w1;
	float r = random * diff;
	wind = w1 + r;
}

float World::getWind()
{
	return wind;
}

void World::drawFloor()
{
	ofSetColor(0, 100, 0);
	ofDrawRectangle(0, ofGetWindowHeight() - 40, ofGetWindowWidth(), 40);
	ofSetColor(255, 255, 255);
}

void World::drawMountain()
{
	mountain.draw(ofGetWindowWidth() / 2.5, ofGetWindowHeight() - 400);
}

ofImage World::getImageMountain()
{
	return mountain;
}

ofVec2f World::getPositionMountain()
{
	ofVec2f positionMountain;
	positionMountain.set(ofGetWindowWidth() / 2.5, ofGetWindowHeight() - 400);
	return positionMountain;
}

World::~World()
{
}
